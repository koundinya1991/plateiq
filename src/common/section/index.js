import React from 'react';
import Style from './style.module.scss';
import PropTypes from 'prop-types';
const Section = ({children}) =>{
  return(
    <div className={Style.section}>
      {children}
    </div>
  )
}

export default Section;

Section.propTypes = {
  children: PropTypes.any,
}