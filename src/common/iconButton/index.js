import React from 'react';
import Style from './style.module.scss';
import PropTypes from 'prop-types';
const IconButton  = ({icon, onClick, height}) => {
  return(
    <div className={Style.rounded} onClick={onClick} style={{height, width: height}}>
      <img className={Style.img} alt={icon} src={icon} style={{height, width: height}}/>
    </div>
  )
}
IconButton.propTypes = {
  icon: PropTypes.string,
  onClick: PropTypes.func,
}
export default IconButton;