import React from 'react';

import Style from './style.module.scss';


class Tabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currTabIndex: 1,
      selectedComp: undefined,
    }
  }
  onTabClick = (tab) => {
    this.setState({
      currTabIndex: tab,
    })
  }

  renderSelectedComp = () => {
    let c = this.props.config.filter(i => i.id === this.state.currTabIndex);
    let comp = c[0];
    return <comp.body/>;
  }
  render() {
    const { config } = this.props;

    return(
      <div className={Style.tabs}>
        <div className={Style.tabsHeader}>
        {config.map(c => {
          let active = null;
          if(c.id === this.state.currTabIndex) {
            active = 'active';
          }
          return <div
          className={`${Style.tab} ${Style[active]}`}
          onClick={()=>this.onTabClick(c.id)}>{c.name}
          </div>
        })}
        </div>
        <div className={Style.tabBody}>{this.renderSelectedComp()}</div>
      </div>

    )
  }
}


export default Tabs;