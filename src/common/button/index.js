import React from 'react';
import Style from './style.module.scss';
import PropTypes from 'prop-types';
const Button  = ({
  text = null,
  type = null,
  onClick = () =>{},
  icon = undefined,
  customStyle = undefined,
  main = false,
}) => {
  console.log('the main is', onClick)
  const imgSrc = icon || undefined;
  let custStyle = customStyle || `${Style.button} ${Style[type]}`;
  console.log('THE CUSTOM STYLE IS', custStyle, Style[type]);
  return(
    <div className={`${custStyle} ${main ? Style.main : null}`}
    onClick={onClick}>
      <div className={Style.text}>{text}</div>
      {imgSrc ? <img className={Style.img} src={imgSrc} alt=''/> : null}
    </div>
  )
}

Button.propTypes = {
  imgSrc: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
}

export default Button;