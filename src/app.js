import React from 'react';
import Style from './app.module.scss';
import Section from './common/section/index';
import FileDataView from './components/fileDataView/index';

function App() {

  return (
    <div className={Style.app}>
      <Section>
        <div className={Style.placeholder}>
            PLATEIQ TEST
        </div>
      </Section>
      <Section>
        <FileDataView/>
      </Section>

    </div>
  );
}

export default App;
