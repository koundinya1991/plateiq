import React from 'react';
import Style from './style.module.scss';
const dummyData = [{
 date: Date.now(),
 flag: 'take_action',
 action: 'Hey, please finish this verification',
 user: 'Koundinay Gavicherla',
}, {
  date: Date.now(),
  flag: 'passed',
  action: 'none',
  user: 'john Doe'
}];


const History = ({
  data = dummyData,
}) => {

  return(
    <div className={Style.container}>
      <div className={Style.header}>Recent Activity</div>
      {data.map(item  => {
          const hours = new Date(item.date).getHours();
          const mm = new Date(item.date).getMinutes();
          const hhmm = `${hours}:${mm}`;

            return(
              <div className={Style.card}>
              <div className={Style.dateTime}>
                <div className={Style.heading}>{new Date(item.date).setHours(0,0,0,0) === new Date(Date.now()).setHours(0,0,0,0) ? 'today' : 'YESTERDAY'}</div>
                <div className={Style.time}>{hhmm}</div>
              </div>
              <div className={Style.action}>
                <div className={Style.actionFlag}>
                  <div className={`${Style.indic} ${Style[item.flag]}`}></div>
                </div>
                <div className={Style.actionDesc}>
                  <div className={Style.user}>{item.user}</div>
                  <div className={Style.desc}>{item.action}</div>
                </div>
              </div>
            </div>
      )})}

    </div>
  )
}

export default History;