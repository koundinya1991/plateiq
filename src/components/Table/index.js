import React from 'react';
import Style from './style.module.scss';
const Table  = ({
  data = [{
    name: 'guyvere-greyezer',
    qty: 10,
    unit_price: '$1 units',
    total: '$10'
  },
    {
      name: 'guyvere-greyezer',
      qty: 10,
      unit_price: '$1 units',
      total: '$10',
  },
    {
      name: 'guyvere-greyezer',
      qty: 10,
      unit_price: '$1 units',
      total: '$10'
    }
  ],
  headers = [{
    name: 'Name',
    key: 'name',
  }, {
    name: 'Quantity',
    key: 'qty',

  }, {
    name: 'Unit Price',
    key: 'unit_price',
  }, {
    name: 'Total',
    key: 'total'
  }],
}) => {

  const renderRow = (arr, row) => {
    let items = arr.map(h => {
      console.log(h);
    return <div className={Style.rowItem}>{row[h]}</div>
  });
    return <div className={`${Style.row}`}>{items}</div>
  }
  const headerArr = headers.map(i => i.key);
  console.log(headerArr);
  return(
    <div className={Style.table}>
      <div className={`${Style.row} ${Style.header}`}>
      {headers.map(h =><div className={`${Style.rowItem}`}>
        {h.name}
      </div>)}
      </div>
      {data.map(d => renderRow(headerArr, d))}
    </div>
  )
}

export default Table;