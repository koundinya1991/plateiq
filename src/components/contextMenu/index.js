import React from 'react';
import Style from './style.module.scss';

export default class ConetxMenu extends React.Component {
  constructor(props) {
    super(props);
    this.contextMenu = React.createRef();
    this.state = {
      posX: 0,
      poxY: 0,
      show: false,
    }
  }

  onBlur = () => {
    this.setState({
      show: false,
    })
  }

  componentDidUpdate(state, props) {
    if(props.posX !== this.props.posX && props.posY !== this.props.posY) {
      this.setState({
        posX: this.props.posX,
        posY: this.props.posY,
        show: true,
      }, () => {
        this.contextMenu.current.focus();
      });
    }
  }



  render() {

    return this.state.show ?(<div tabIndex={0} className={Style.contextMenu} ref={this.contextMenu} style={{
      top: this.state.posY+10 || 0,
      left: this.state.posX-10 || 0,
    }} onBlur={this.onBlur}>
      <div className={Style.item}>Export</div>
      <div className={Style.item}>Mark As Exported</div>
      <div className={Style.item}>Set Up Vendor</div>
      <div className={Style.item}>Flag Invoice</div>
      <div className={Style.item}>Archive Invoice</div>
      <div className={Style.item}>Delete Archive</div>
    </div>): null;
  }
}