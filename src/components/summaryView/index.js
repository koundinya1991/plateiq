import React from 'react';
import Style from './style.module.scss';
/**
 * label - a helper component to show values with a label
 * @param {object} props
 */
const Label = ({name, value, type=undefined}) => {
  return (<div className={`${Style.label} ${Style[type]}`}>
    <div className={Style.name}>{name}</div>
    <div className={Style.value}>{value}</div>
  </div>)
}
/**
 * this component is the one which opens when down arrow is clicked
 */
const SummaryView = () => {
  return(
    <div className={Style.summaryView}>
      <div className={Style.row}>
        <Label name={'invice no.'} value={'00003245'}/>
        <Label name={'Invoice Type'} value={'reciept'}/>
        <Label name={'Restaurant'} value={"Monty's cheese shop"}/>
        <div className={Style.col}>
        <Label type="horizontal" name={'Subtotal'} value={'$1300.00'}/>
        <Label type="horizontal" name={'tax'} value={'0$26.32'}/>
        </div>

      </div>
      <div className={Style.row}>
        <Label name={'Posting date'} value={'Jul 14, 2017'}/>
        <Label name={'Invoice date'} value={'Jul 14, 2017'}/>
        <Label name={'Due date'} value={'Jul 14, 2017'}/>
        <Label type="horizontal" name={'total'} value={'$ 10,000.00'}/>
      </div>
    </div>
  )
}

export default SummaryView;