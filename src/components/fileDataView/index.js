import React, { useState } from 'react';
import Style from './style.module.scss';
import Button from '../../common/button/index.js';
import icon from '../../images/envelope.svg';
import down from '../../images/down-chevron.svg';
import IconButton from '../../common/iconButton/index';
import check from '../../images/check.svg';
import SummaryView from '../../components/summaryView/index';
import ContextMenu from '../contextMenu/index';
import Table from '../Table/index';

import Tabs from '../../common/tabs/index';
import History from '../../components/history/index';
const LineItems = () => <Table/>;


/**
 *  The main file data vuew when user uploades and following info is shown, this is right section of the page
 * @param {object} props
 */
const FileDataView = (props) => {
  const [expand, onExpand ] = useState(false);

  const [posX, onXChange] = useState(0);
  const [posY,onYChange] = useState(0);
  const onShowDropDown = (e) => {
    console.log('ELLLLLLLLLLLLLOOOOOOO', e.target);
    let left = e.clientX;
    let top = e.clientY;
    console.log('the left and top is',left, top);
    onXChange(left);
    onYChange(top);
  }
  let buttonStyle;
  if(expand) {
    buttonStyle = {
      transform: `rotate(180deg)`
    }
  } else {
    buttonStyle = {
      transform: `rotate(0)`
    }
  }
  return(
    <div className={Style.container}>
      <div className={Style.row}>
        <Button
        text={'direct'}
        customStyle={Style.directButton}
        icon={icon}

        />

        <div className={Style.buttonGroup}>
          <Button
          text={'More'}
          icon={down}
          onClick={(e) => onShowDropDown(e)}
          />
          <Button
          text={'Approve'}
          type={'main'}
          customStyle={Style.approveButton}
          />
        </div>
      </div>
          {expand ? <span className={Style.label}>Vendor</span>: null}
          <div className={`${Style.row} ${Style.noPadding}`}>
            <span className={Style.row}><h2>Ferguesia Cheese</h2>
            <div style={buttonStyle}>
              <IconButton icon={down} onClick={() => onExpand(!expand)}/>
            </div>
            </span>
          </div>
          {expand ? <div className={Style.caption}><IconButton icon={check} height={"5px"}/> this vendor is mapped</div>: null}

        {!expand ? <div className={Style.row}>
          <div>0000352</div>
          <div>19/02/2020</div>
          <div>Monty's Cheese shop</div>
          <div> details</div>
          <div className={Style.row}>
            <div className={Style.caption}>TOTAL</div>
            <div className={Style.value}>$ 10,000.00</div>
          </div>
        </div>: null}
        {expand ? <SummaryView/>: null}
        <ContextMenu posX={posX} posY={posY}></ContextMenu>

          <Tabs
          currTabIndex={1}
          config={
            [{
              id: 1,
              name: 'Line Items',
              body: () => <LineItems/>
            },
            {
              id: 2,
              name: 'History',
              body: () => <History/>
            }

          ]}/>

    </div>
  )

}

export default FileDataView;